# python3

# Context file to enable testing as recommended by Kenneth Reitz at:
# http://docs.python-guide.org/en/latest/writing/structure/

import os
import sys

basedir = os.path.abspath(os.path.join(os.path.dirname(__file__),'..'))
sys.path.insert(0, basedir)
sys.path.insert(1, os.path.join(
    basedir,
    "thirdparty",
    "behave-1.2.6-py2.py3-none-any.whl"
))
import trailer