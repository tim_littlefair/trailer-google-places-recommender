# google_places_client.py
# This module defines the PlacesClient class,
# The purpose of this class is to contain all knowledge of
# the Google Places API's scheme of URL parameters and 
# the content of requests from that API.
# This module relies on the proxy module to perform web
# requests and caching.

import os
import urllib
import json
import re
import sys
import logging

logger = logging.getLogger("search_controller")
logger.setLevel(logging.INFO)

from .great_circle_distance import great_circle_distance_in_metres
from .simple_trail import Zone as ZoneCandidate

from .proxy import ProxyFactory

from .google_api_client import GoogleApiClient, GEOMETRY_KEY, LOCATION_KEY, LAT_KEY, LNG_KEY, TYPES_KEY
from .google_api_client import EXCLUDED_PLACE_TYPES

# the next item defines the minimum visual radius of a zone
# on a map.  This is also used as the radius of zones which 
# have a centre but no bounds.
MINIMUM_ZONE_RADIUS = 50

# The next item defines the maximum visual radius of a zone
# on a map.
MAXIMUM_VISUAL_ZONE_RADIUS = 200

# The next item defines a threshold for radius beyond 
# which a zone is automatically discarded as too large
MAXIMUM_ABSOLUTE_ZONE_RADIUS = 1000

# An extra attribute which is added to record the prominence
# of each entry
PROMINENCE_KEY = "SearchController.PROMINENCE"

class SearchController :
    def __init__(self,api_client):
        self.api_client = api_client
        # The candidate_results dictionary will store sufficient
        # information for prominence of each candidate to be assessed
        # It will be populated by the get_candidates method using the 
        # results of a Google Places 'Nearby' search or similar
        self.candidate_results = {}
    
    def reset(self):
        self.candidate_results = {}
        
    def _combine_result_arrays(self,result_arrays):
        results = []
        initial_ra_count = len(result_arrays)
        while len(result_arrays) > 0:
            a = result_arrays[0]
            result_arrays = result_arrays[1:]
            if len(a)==0:
                continue
            else:
                logger.debug("Result arrays: %d this array: %d results: %d" %
                    ( len(result_arrays), len(a), len(results))
                )
                while a[0] in results:
                    a = a[1:]
                    if len(a)==0:
                        break
                if len(a)>0 and a[0] not in results:
                    results += [ a[0] ]
                    a = a[1:]
                result_arrays += [ a ]
        logger.debug("_combine_result_arrays: %d arrays combined to give %d candidates" %(
            initial_ra_count, len(results)
        ))
        return results

    def _default_prominence_fn(self,type_prominences):
        type_prominence = 1.0
        candidate_decay_factor = 0.9
        type_decay_factor = 0.5
        item_prominence = 0.0
        for tp in type_prominences:
            item_prominence+=type_prominence*candidate_decay_factor**tp[1]
            type_prominence*=type_decay_factor
        return item_prominence

    def _create_prominence_map(self,client, result_arrays, requested_types):
        if requested_types is None:
            requested_types = [ "*" ]
        assert len(requested_types) == len(result_arrays)
        result_prominence_map = {}
        for type_index in range(0,len(requested_types)):
            for candidate_index in range(0,len(result_arrays[type_index])):
                candidate_uid = client.uid_for_result(result_arrays[type_index][candidate_index])
                candidate_name = client.name_for_result(result_arrays[type_index][candidate_index])
                if candidate_uid in result_prominence_map.keys():
                    candidate_entry = result_prominence_map[candidate_uid]
                    candidate_entry[2] += [
                        ( 
                            requested_types[type_index],
                            candidate_index+1, 
                            len(result_arrays[type_index]), 
                        ),
                    ]
                    candidate_entry[3]=self._default_prominence_fn(
                        candidate_entry[2]
                    )
                else:
                    candidate_entry = {}
                    candidate_entry[0] = candidate_uid
                    candidate_entry[1] = candidate_name
                    candidate_entry[2] = [
                        ( 
                            requested_types[type_index],
                            candidate_index+1, 
                            len(result_arrays[type_index]), 
                        ),
                    ]
                    candidate_entry[3]=self._default_prominence_fn(
                        candidate_entry[2]
                    )
                for excluded_type in EXCLUDED_PLACE_TYPES:
                    if excluded_type in self.candidate_results[candidate_entry[0]][TYPES_KEY]:
                        candidate_entry[3]=0.0
                result_prominence_map[candidate_uid]=candidate_entry
        logger.debug("Prominence map has %d entries"% ( len(result_prominence_map.keys())))
        return result_prominence_map

    def get_candidates(self, locality_name, requested_types):
        lat, lng, radius = self.api_client.get_locality_latlngrad(locality_name)
        candidate_result_arrays = self.api_client.get_places_near_latlng(
            lat,lng,radius,requested_types
        )
        combined_result_array = self._combine_result_arrays(candidate_result_arrays)
        for r in combined_result_array:
            self.candidate_results[self.api_client.uid_for_result(r)]=r
        prominence_result_map = self._create_prominence_map(
            self.api_client, candidate_result_arrays,requested_types
        )
        prominence_entries = []
        while len(prominence_result_map)>0:
            k,v = prominence_result_map.popitem()
            self.candidate_results[k][PROMINENCE_KEY] = v[3]
            prominence_entries += [ (v[3],v[1],v[2],), ]
        for e in sorted(prominence_entries, reverse=True):
            logger.debug("%6.4f %-40s %s" % (e))
        return sorted(self.candidate_results.keys())
    
    def get_coordinates(self,cid):
        locn = self.candidate_results[cid][GEOMETRY_KEY][LOCATION_KEY]
        return ( float(locn[LAT_KEY]), float(locn[LNG_KEY]) )
        
    def get_details(self,selected_candidate_list):
        retval = []
        for candidate_id in selected_candidate_list:
            candidate_details = self.api_client.get_place_details(candidate_id)
            retval += [ candidate_details ]
        return retval

    def distance_between_candidates(self,cid1, cid2):
        distance=great_circle_distance_in_metres(
            self.get_coordinates(cid1),
            self.get_coordinates(cid2)
        )
        logger.debug("%s->%s = %d m"%(cid1,cid2,distance))
        return distance

    
    def prominence_of_candidate( self, cid ):
        return self.candidate_results[cid][PROMINENCE_KEY]

import unittest

from .google_api_client import NEWCASTLE, DURHAM, GOTHENBURG, DEFAULT_PLACE_TYPES

class SearchControllerTest(unittest.TestCase) :

    def __init__(self,*args, **kwargs):
        self.dir = kwargs.pop("dir",".")
        self.cache_filename = kwargs.pop(
            "cache_filename",
            os.path.join(self.dir,"test.cache")
        )
        super(SearchControllerTest, self).__init__(*args, **kwargs)
         
    def setUp(self):
        self.proxy=ProxyFactory(self.cache_filename).create_proxy()
        self.client = GoogleApiClient(self.proxy)
        self.controller = SearchController(self.client)
    
    def tearDown(self):
        self.controller = None
        self.client = None
        self.proxy = None

    def test_get_candidates(self):
        locname = NEWCASTLE[2]
        # art_gallery is not in the default set of types as it 
        # tends to get spammed onto places which are not interesting.
        # We add it here because the list of returned places will 
        # overlap with museum and ensure that the code for dealing
        # with locations associated with more than one requested type 
        # gets covered
        requested_types = DEFAULT_PLACE_TYPES + ( "art_gallery", )
        candidate_list = self.controller.get_candidates(locname,requested_types)
        self.assertTrue(len(candidate_list)>0)
        for cid in candidate_list:
            self.assertEqual(str,type(cid))

    def test_get_details(self):
        locname = NEWCASTLE[2]
        candidate_list = self.controller.get_candidates(locname,None)
        candidate_details = self.controller.get_details(candidate_list)
        self.assertTrue(len(candidate_list)==len(candidate_details))
        for cd in candidate_details:
            self.assertEqual(dict,type(cd))
        
    def test_distance_between_candidates(self):
        locname = NEWCASTLE[2]
        candidate_list = self.controller.get_candidates(locname,DEFAULT_PLACE_TYPES)
        for cid1 in candidate_list:
            for cid2 in candidate_list:
                dist = self.controller.distance_between_candidates(cid1,cid2)
                self.assertEqual(float,type(dist))
                if cid1 == cid2:
                    self.assertEqual(0.0,dist)
                else:
                    self.assertLessEqual(0.0,dist)

        candidate_details = self.controller.get_details(candidate_list)
        self.assertTrue(len(candidate_list)==len(candidate_details))
        for cd in candidate_details:
            self.assertEqual(dict,type(cd))
    
    def test_prominence_of_candidate(self):
        locname = NEWCASTLE[2]
        candidate_list = self.controller.get_candidates(locname,DEFAULT_PLACE_TYPES)
        for cid in candidate_list:
            prominence = self.controller.prominence_of_candidate(cid)
            self.assertEqual(float,type(prominence))
            self.assertGreater(len(DEFAULT_PLACE_TYPES),prominence)
        
    def runTest(self):
        logger.info("Running unit tests from class SearchControllerTest")
        self.test_get_candidates()
        self.test_get_details()
        self.test_distance_between_candidates()
        self.test_prominence_of_candidate()

if __name__ == "__main__" :
    from unittest.runner import TextTestRunner
    logger.setLevel(logging.INFO)
    #testlogstr = logging.StreamHandler()
    #testlogstr.setLevel(logging.INFO)
    #logger.addHandler(testlogstr)

    runner = TextTestRunner()
    runner.run(SearchControllerTest())
