from context import trailer

import logging
from unittest.runner import TextTestRunner
from unittest.suite import TestSuite

logger = logging.getLogger()
logger.setLevel(logging.INFO)

suite = TestSuite()

# The proxy uses a different cache as it is destroyed and recreated during test execution
suite.addTest(trailer.proxy.ProxyTest(dir="./_test",cache_filename="proxy.test.cache"))

suite.addTest(trailer.google_api_client.GoogleApiClientTest(dir="./_test"))
suite.addTest(trailer.search_controller.SearchControllerTest(dir="./_test"))
suite.addTest(trailer.recommender.RecommenderTest(dir="./_test"))
suite.addTest(trailer.itinerary.ItineraryTest(dir="./_test"))

runner = TextTestRunner()
runner.run(suite)

