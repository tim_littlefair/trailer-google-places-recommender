#!/bin/sh

if [ "$1" == "--clean" ]
then
    rm -f python/*.cache
    rm -f generated_trails/*/*
    rm -f generated_trails/*.kml generated_trails/*.tqz
    shift 
else
    rm -rf generated_trails/*
    mkdir generated_trails/day_zero_trails
    mkdir generated_trails/recommender_tests
    mkdir generated_trails/itinerary_tests
fi

cd python
export PYTHONPATH=../thirdparty/behave-1.2.6-py2.py3-none-any.whl
python3.5 -m coverage erase

if [ -f "$1" ] 
then
    python3.5 -m coverage run $1
else
    python3.5 -m coverage run simple_trail.py
    python3.5 -m coverage run --append proxy.py
    python3.5 -m coverage run --append google_api_client.py
    python3.5 -m coverage run --append search_controller.py
    python3.5 -m coverage run --append itinerary.py
    python3.5 -m coverage run --append recommender.py
    python3.5 -m coverage run --append day_zero_trails.py
fi

python3.5 -m coverage html --include=./*.py
python3.5 -m coverage report -m --include=./*.py
cd ..

iosapp_trails=../trailer.2/iosapp/TW1/trails
rm $iosapp_trails/*.tqz
cp generated_trails/day_zero_trails/*.tqz $iosapp_trails
