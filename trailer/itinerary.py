import sys
import traceback
import binascii
import logging
import unittest
import os

from .simple_trail import TrailFactory, generate
from .proxy import ProxyFactory

from .template_tqz import TqzTemplate
from .template_kml import KmlTemplate

logger = logging.getLogger("itinerary")

MAX_LOCALITY_RADIUS = 10000.0
MINIMUM_ZONE_RADIUS = 150.0
MAX_ZONE_RADIUS = 400.0

# from great_circle_distance import great_circle_distance_in_metres

from .google_api_client import GoogleApiClient, DEFAULT_PLACE_TYPES

# Importing the python-cluster library
from cluster import HierarchicalClustering

def generate_itinerary_trail(itinerary_name, waypoints, common_suffix=None):
    proxy_factory = ProxyFactory("itinerary.cache")
    proxy = proxy_factory.create_proxy()
    client = GoogleApiClient(proxy)
    waypoint_list = []
    for wp in waypoints:
        if common_suffix is not None : 
            wp += common_suffix 
        wp_locality_id = client.get_locality_id(wp)
        wp_locality_details = client.get_place_details(wp_locality_id)
        waypoint_list += [ wp_locality_details ]
    trail_factory = TrailFactory()
    t = trail_factory.create_trail(itinerary_name)
    for wp_info in waypoint_list:
        attrs = client.zone_attrs_for_result(wp_info)
        trail_factory.add_zone_to_trail(
            t, *attrs
        )
    return t

class ItineraryTest(unittest.TestCase) :

    def __init__(self,*args, **kwargs):
        self.dir = kwargs.pop("dir",".")
        self.cache_filename = kwargs.pop(
            "cache_filename",
            os.path.join(self.dir,"test.cache")
        )
        super(ItineraryTest, self).__init__(*args, **kwargs)

    def setUp(self):
        pass
    def tearDown(self):
        pass
    def test_generate_itinerary_trail(self):
        itinerary_trail = generate_itinerary_trail(
            "Live Music Venues in London",
            [
                "Dingwalls",
                "The 100 Club",
                "The Mean Fiddler",
                "The Town and Country Club",
                "The Hackney Empire",
                "The Royal Albert Hall",
            ],
            common_suffix = ", London, UK"
        )
        generate(itinerary_trail,TqzTemplate(),os.path.join(self.dir,"generated","itinerary"))
        generate(itinerary_trail,KmlTemplate(),os.path.join(self.dir,"generated","itinerary"))
        
    def runTest(self):
        logger.info("Running unit tests from class RecommenderTest")
        self.test_generate_itinerary_trail()

if __name__ == "__main__" :

    # """ 
    # First run the unit tests of this and other packages ...
    from unittest.runner import TextTestRunner
    logger.setLevel(logging.INFO)
    runner = TextTestRunner()
    runner.run(ItineraryTest())
    
