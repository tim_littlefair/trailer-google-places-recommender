import os
import sys
import traceback
import binascii
import logging
import unittest

from .simple_trail import TrailFactory, generate
from .proxy import ProxyFactory

from .template_tqz import TqzTemplate
from .template_kml import KmlTemplate

logger = logging.getLogger("recommender")

MAX_LOCALITY_RADIUS = 10000.0
MINIMUM_ZONE_RADIUS = 150.0
MAX_ZONE_RADIUS = 400.0

# from great_circle_distance import great_circle_distance_in_metres

from .google_api_client import GoogleApiClient, DEFAULT_PLACE_TYPES
from . import search_controller

# Importing the python-cluster library
from cluster import HierarchicalClustering

# The cluster library logs noisily at INFO level
logging.getLogger("cluster.matrix").setLevel(logging.WARNING)
logging.getLogger("cluster.method.hierarchical").setLevel(logging.WARNING)

class Recommender :
    def __init__(self,trail_factory,proxy_factory,key_file=None, ) :
        self.trail_factory = trail_factory
        self.proxy = proxy_factory.create_proxy()
        self.client = GoogleApiClient(self.proxy)
        self.search_controller = search_controller.SearchController(self.client)

    def calculate_cluster_centre_coordinates(self,cluster):
        cluster_lat = sum( 
            self.search_controller.get_coordinates(cid)[0] 
            for cid in cluster
        ) / len(cluster)
        cluster_lng = sum(
            self.search_controller.get_coordinates(cid)[1] 
            for cid in cluster
        ) / len(cluster)
        return ( cluster_lat, cluster_lng )

    def truncate_candidate_list_to_max_length_and_sum_prominence(
        self,
        candidates,
        max_items_per_trail
    ):
        if len(candidates)>max_items_per_trail:
            candidate_id_to_prominence = (
                lambda cid: self.search_controller.prominence_of_candidate(cid)
            )
            candidates = sorted(candidates,key=candidate_id_to_prominence,reverse=True)
            candidates=candidates[0:max_items_per_trail]
        prominence = sum (
            self.search_controller.prominence_of_candidate(cid)
            for cid in candidates
        )
        logger.debug("Prominence: %6.4f Candidates: %s"%(
            prominence,", ".join(candidates)
        ))
        return candidates, prominence
        
    def generate_clustered_trails(
        self, 
        search_locality_name, 
        types=None, 
        max_items_per_trail = None,
        search_radius_metres = None,
        cluster_radius_metres = None,
        max_candidates = None
    ) :
        # Clear candidate results from prior searches
        self.search_controller.reset()
        if types is None : types = DEFAULT_PLACE_TYPES
        if max_items_per_trail is None : max_items_per_trail = 25
        if cluster_radius_metres is None : cluster_radius_metres = 3000
        min_average_prominence_for_trail = 0.3
        candidate_id_list = self.search_controller.get_candidates(search_locality_name,types)
        distance_func = lambda cid1,cid2 : self.search_controller.distance_between_candidates(cid1,cid2)
        cl = HierarchicalClustering(candidate_id_list, distance_func)
        clusters = cl.getlevel(cluster_radius_metres)
        candidates_from_discarded_clusters = []
        trails = [] 
        logger.debug("Number of candidates: %d, number of clusters: %d" % (
            len(candidate_id_list), len(clusters),
        ))
        cluster_name_map = {}
        # First pass - see if there are any clusters we should 
        # merge
        for c in sorted(clusters,key=len, reverse=True) :
            assert list == type(c)
            cluster_lat, cluster_lng = self.calculate_cluster_centre_coordinates(c)
            cluster_locality_name = self.client.get_latlng_placename(cluster_lat, cluster_lng)
            if cluster_locality_name is None:
                # If we couldn't geocode a name, we just discard the cluster.
                logger.debug(
                    "cluster centred at %.8f,%.8f, %d candidates: No locality name, discarding" %(
                        cluster_lat, cluster_lng, len(c) 
                    )
                )
                candidates_from_discarded_clusters += c
                continue
            elif cluster_locality_name in cluster_name_map:
                # We have already found a cluster which geocodes to the same name,
                # merge the new one into the old one
                logger.debug(
                    "cluster centred at %.8f,%.8f, %d candidates: Geocodes to existing name %s, discarding" %(
                        cluster_lat, cluster_lng, len(c), cluster_locality_name 
                    )
                )
                candidates_from_discarded_clusters += c
            else:
                logger.debug(
                    "cluster centred at %.8f,%.8f, %d candidates: New cluster with locality name %s" %(
                        cluster_lat, cluster_lng, len(c), cluster_locality_name 
                    )
                )
                cluster_name_map[cluster_locality_name] = c
        # Second pass, generate trails from the named clusters
        cluster_name_to_length = lambda cn: len(cluster_name_map[cn])
        for cname in sorted(cluster_name_map.keys(),reverse=True,key=cluster_name_to_length):
            c = cluster_name_map[cname]
            cluster_lat, cluster_lng = self.calculate_cluster_centre_coordinates(c)
            c, cluster_prominence = self.truncate_candidate_list_to_max_length_and_sum_prominence(
                c, max_items_per_trail
            )
            if (
                len(c)<5 or
                cluster_prominence/len(c)<min_average_prominence_for_trail
            ):
                logger.debug(
                    "Not creating trail for %s, %d zones, prominence %6.4f" % (
                        cname, len(c), cluster_prominence
                    )
                )
            else:
                logger.info(
                    "Creating trail for %s, %d zones, prominence %6.4f" % (
                        cname, len(c), cluster_prominence
                    )
                )
                # Get full details on the candidates we want to use
                candidate_details = self.search_controller.get_details(c)
                t = self.trail_factory.create_trail(
                    cname, cluster_lat, cluster_lng)
                for z in candidate_details:
                    if z is None:
                        continue
                    attrs = self.client.zone_attrs_for_result(z)
                    self.trail_factory.add_zone_to_trail(
                        t, *attrs
                    )
                trails += [ t ]
        # Work out whether there was anything worthwhile in
        # the discarded clusters
        c, prominence = self.truncate_candidate_list_to_max_length_and_sum_prominence(
            candidates_from_discarded_clusters, max_items_per_trail
        )
        if (
            len(c)<5 or
            prominence/len(c)<min_average_prominence_for_trail
        ):
            pass
        else:
            cname = search_locality_name + " (surrounding area)"
            logger.info(
                "Creating trail for %s, %d zones, prominence %6.4f" % (
                    cname, len(c), prominence
                )
            )
            cluster_lat, cluster_lng = self.calculate_cluster_centre_coordinates(c)
            t = self.trail_factory.create_trail(
                cname, cluster_lat, cluster_lng)
            candidate_details = self.search_controller.get_details(c)
            for z in candidate_details:
                if z is None:
                    continue
                attrs = self.client.zone_attrs_for_result(z)
                self.trail_factory.add_zone_to_trail(
                    t, *attrs
                )
            # Reset the radius by creating a new client
            # self.client = GoogleApiClient(self.proxy)
            trails += [ t ]
        return trails       

class RecommenderTest(unittest.TestCase) :

    def __init__(self,*args, **kwargs):
        self.dir = kwargs.pop("dir",".")
        self.cache_filename = kwargs.pop(
            "cache_filename",
            os.path.join(self.dir,"test.cache")
        )
        super(RecommenderTest, self).__init__(*args, **kwargs)

    def setUp(self):
        self.recommender = Recommender(
            TrailFactory(), ProxyFactory(self.cache_filename)
        )
    def tearDown(self):
        self.recommender = None
    def test_generate_clustered_trails(self):
        trails = self.recommender.generate_clustered_trails(
            "Perth, Western Australia", 
            search_radius_metres=25000
        )
        for t in trails :
            gendir = os.path.join(self.dir,"generated","recommender")
            generate(t,TqzTemplate(),gendir)
            generate(t,KmlTemplate(),gendir)
        
    def runTest(self):
        logger.info("Running unit tests from class RecommenderTest")
        self.test_generate_clustered_trails()

if __name__ == "__main__" :

    # """ 
    # First run the unit tests of this and other packages ...
    from unittest.runner import TextTestRunner
    logger.setLevel(logging.INFO)
    runner = TextTestRunner()
    runner.run(RecommenderTest())
    
