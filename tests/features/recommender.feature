Feature: Recommender
In order to make the most of my time
as a traveller visiting a new place
I want to find the key cultural locations and transport hubs

Scenario Outline: Simple search
Given the city name '<city_name>'
when I request a list of <poi_count> places
then the list returned should contain <poi_count> zones
and the list returned should include zone <poi_name> 
Examples:
| city_name            | poi_count | poi_name           |
| Rotorua, New Zealand | 8         | Kuirau Park        |
    
Scenario Outline: Cluster search
Given the city name '<city_name>'
when I request a set of trails within 50 km of the city
and ask for the each trail to contain up to 10 zones clustered within a radius of <radius> metres
then the list returned should include trails of districts <cluster1_name> and <cluster2_name>
and the trail for <cluster1_name> should include a zone called <zone1_name>
and the trail for <cluster2_name> should include a zone called <zone2_name>
Examples:
| city_name                | radius | cluster1_name | zone1_name     | cluster2_name | zone2_name      |
| Perth, Western Australia | 1500   | Northbridge   | Russell Square | Fremantle     | Maritime Museum |

