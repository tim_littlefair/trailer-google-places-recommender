# python3
# template_kml.py
# Script to convert a trail object into the KML format
# consumed by Google Maps

from .template_utils import sanitize

_TRAIL_KML_TEMPLATE = """<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2">
  <Document>
    <name>%s</name>
    <description>%s</description>
    <Folder>
%s
    </Folder>
  </Document>
</kml>
"""

_ZONE_KML_TEMPLATE = """      <Placemark>
        <name>%s</name>
        <description>%s</description>
        <Point>
          <coordinates>%.8f,%.8f,0</coordinates>
        </Point>
      </Placemark>
"""

class KmlTemplate:
    def __init__(self):
        pass
        
    def to_bytes(self,t):
        zone_node_list = []
        for z in t.zones :
            zone_node = _ZONE_KML_TEMPLATE % (
                sanitize(z.name), sanitize(z.message), z.lng, z.lat,
            )
            zone_node_list += [ zone_node ]
        trail_kml_string = _TRAIL_KML_TEMPLATE % (
            sanitize(t.locality_name), sanitize(t.description), 
            "".join(zone_node_list)
        )
        return bytes(trail_kml_string,'utf-8')

    def to_file(self, t, dirpath_and_basename):
        f = open( dirpath_and_basename + ".kml", mode='wb' )
        f.write(self.to_bytes(t))
        f.close()

