# Makefile for the trailer recommendation generation logic.
# 
# This library is associated with the Trailer applications 
# for iOS, Android and the web described at 
# http://tl-trailer.blogspot.com.au/

# This Makefile is being developed on macOS High Sierra 10.13.4 in May 2018.
# I would hope that it would work on Linux as well, but I can't commit to 
# test it there often.
# Windows support is TODO.  When I get to it I am not sure whether it 
# will use this file or a separate mechanism.

# This Makefile uses Kenneth Reitz's pipenv module, and is intended
# to help the project it is a part of conform to Kenneth's recommendations
# for Python project structure.
# https://docs.pipenv.org/
# http://docs.python-guide.org/en/latest/writing/structure/

# Tested on Python 3.5, expected to work on 3.6+ higher versions, 
# may work on Python 3.4, is expected to fail on Python 2.X, 
# 3.0, 3.1, 3.2 or 3.3.
PYTHON=python3.5

# As of May 2018, there has been a transition in hosting arrangements for
# PyPi which, combined with the age of the platform-provided OpenSSL
# libraries on macOS, lead to some of the pip operations underlying
# pipenv failing with a message which mentions TLSV1_ALERT_PROTOCOL_VERSION
# If this happens, run the command 'make fix_pypi_access' to use the 
# target immediately below this comment, which is based on recipes for this 
# problem recommended at:
# https://stackoverflow.com/questions/49748063/pip-install-fails-for-every-package-could-not-find-a-version-that-satisfies-th
# https://stackoverflow.com/a/49282825
# If the fix works, it will permanently modify the system's python installation
# referenced by symbol PYTHON.
# Hopefully nobody minds the problem being fixed for other packages as well if
# it works.  
# Hopefully no harm done if it doesn't work.
_fix_pypi_access:
	curl https://bootstrap.pypa.io/get-pip.py | sudo -H $(PYTHON)
	sudo -H $(PYTHON) -m pip install --upgrade requests[security]
	sudo -H $(PYTHON) -m pip install --upgrade setuptools

_suggest_fix_pypi:
	echo Installation of pipenv failed despite user having
	echo sudo rights.
	echo Try 'make _fix_pypi_access' if the error messages
	echo mentioned TLSV1_ALERT_PROTOCOL_VERSION

_get_pipenv:
	echo Need to install Python module pipenv
	echo Prompting for user password for sudo rights 
	echo to modify system installation of $(PYTHON) 
	echo sudo true || echo User may not have right to use sudo
	sudo -H $(PYTHON) -m pip install pipenv || make _suggest_fix_pypi

# The development environment setup also modifies the system's python
# installation to add Kenneth Reitz's pipenv module.
# After this module is added (with its dependencies), additional 
# dependency modules for the current project will be added
# to a local virtual environment, and will not affect the system installation
.has_pipenv:
	-sudo -H $(PYTHON) -m pipenv --help > /dev/null || make _get_pipenv
	touch .has_pipenv

init: .has_pipenv 
	$(PYTHON) -m pipenv install requests cluster behave coverage
	$(PYTHON) -m pipenv update requests cluster behave coverage

test:
	@if [ ! -d _test/generated/recommender ] ; then mkdir -p _test/generated/recommender ; fi
	@if [ ! -d _test/generated/itinerary ] ; then mkdir -p _test/generated/itinerary ; fi
	@if [ ! -d _test/generated/samples ] ; then mkdir -p _test/generated/samples ; fi
	$(PYTHON) -m pipenv run $(PYTHON) -m coverage run tests/test_coverage.py
	$(PYTHON) -m pipenv run $(PYTHON) -m coverage html --include=./*.py
	$(PYTHON) -m pipenv run $(PYTHON) -m coverage report -m --include=./*.py
	$(PYTHON) -m pipenv run $(PYTHON) tests/sample_sweden_and_norway.py
	$(PYTHON) -m pipenv run $(PYTHON) tests/sample_uk_belgium_and_netherlands.py

.PHONY: _fix_pypi_access _suggest_fix_pypi _get_pipenv init test
