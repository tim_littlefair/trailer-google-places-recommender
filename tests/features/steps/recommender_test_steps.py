import sys
import os
import logging

from recommender import Recommender
from proxy import Proxy, enable_debug as proxy_enable_debug
import simple_trail

from behave import given, when, then


def extend_description(previous_description, heading,item_list) :
    if len(item_list) > 0 :
        item_string = unicode(heading) + u': ' + u', '.join(item_list)
    else :
        item_string = None
    if item_string and previous_description :
        return unicode(item_string) + u'&#10;' + item_string
    elif item_string :
        return item_string
    else :
        return previous_description
        
class ProxyFactory :
    def create_proxy(self,*args):
        proxy = Proxy()
        # proxy_enable_debug()
        proxy.load_cache("recommender_feature.cache")
        return proxy

the_recommender = Recommender(simple_trail.TrailFactory(),ProxyFactory())

@given(u'the city name \'{city_name}\'')
def step_impl(context,city_name) :
    context.city_name = city_name 
    context.terms = ( )
    assert True

@when(u'I request a list of {poi_count} places')
def step_impl(context,poi_count) : #@DuplicatedSignature
    context.num_items = int(poi_count)
    context.search_type = 'simple'
    assert True
 
@when(u'ask for the places to be within a radius of {radius} metres')
def step_impl(context,radius): #@DuplicatedSignature
    context.search_type = 'cluster'
    context.radius = int(radius)
    assert True
    
@then(u'the list returned should contain {poi_count} zones')
def step_impl(context,poi_count) : #@DuplicatedSignature
    assert context.search_type == 'simple'
    context.trail = the_recommender.generate_trail_from_locality_name(
        context.city_name,
        num_items=int(poi_count)
    )
    simple_trail.generate_tqz(context.trail,'../generated_trails',context.search_type)
    assert len(context.trail.zones)==int(poi_count)

@then(u'the list returned should include zone {poi_name}')
def step_impl(context,poi_name) : #@DuplicatedSignature
    context.zone = context.trail.find_zone(poi_name)
    assert context.zone is not None

@then(u'the list should not include zone {poi_name}')
def step_impl(context,poi_name):
    context.zone = None
    context.zone = context.trail.find_zone(poi_name)
    assert context.zone is None
    
@when(u'I request a set of trails within {search_radius} km of the city')
def step_impl(context,search_radius):
    context.search_type = 'cluster'
    context.search_radius_km = float(search_radius)

@when(u'ask for the each trail to contain up to {poi_count} zones clustered within a radius of {cluster_radius} metres')
def step_impl(context,poi_count, cluster_radius):
    assert context.search_type == 'cluster'
    context.cluster_trails = the_recommender.generate_clustered_trails(
        search_locality_name = context.city_name,
        max_items_per_trail=int(poi_count),
        search_radius_metres=float(context.search_radius_km)*1000.0,
        cluster_radius_metres=float(cluster_radius)
    )
    assert len(context.cluster_trails) > 0
    for t in context.cluster_trails :
        simple_trail.generate_tqz(t,"../generated_trails")
    
@then(u'the list returned should include trails of districts {cluster1_name} and {cluster2_name}')
def step_impl(context,cluster1_name,cluster2_name) : #@DuplicatedSignature
    clusters1 = filter(lambda t: unicode(cluster1_name) in t.locality_name, context.cluster_trails)
    assert len(clusters1)==1
    clusters2 = filter(lambda t: unicode(cluster2_name) in t.locality_name, context.cluster_trails)
    assert len(clusters2)==1
 
@then(u'the trail for {trail_name} should include a zone called {zone_name}')
def step_impl(context,trail_name, zone_name): #@DuplicatedSignature
    trail_name_matches = lambda t: unicode(trail_name) in unicode(t.locality_name)
    zone_name_matches = lambda z: unicode(zone_name) in unicode(z.name)
    ( trail, ) = filter(trail_name_matches, context.cluster_trails)
    ( zone, ) =  filter(zone_name_matches, trail.zones)
    assert zone is not None

    


