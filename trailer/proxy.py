import urllib
import urllib.request
import urllib.parse
import time
import logging

logger = logging.getLogger("proxy")

def _get_response_via_urllib2(url):
    remaining_tries = 3
    resp = None
    time.sleep(0.200)
    while remaining_tries > 0 :
        try :
            resp = str(urllib.request.urlopen(url).read(),'utf-8')
            break
        except KeyboardInterrupt:
            remaining_tries = remaining_tries - 1
            time.sleep(2.000)
    return resp

debug_cache = False   

def enable_debug() :
    global debug_cache
    debug_cache = True
    logger.setLevel(logging.DEBUG)

class Proxy :
    def __init__(self):
        self.prefixes = {}
        self.cache = {}
        self.cache_filename = None  
        self.cache_misses = 0

    def _cache_debug_message(self,outcome,url,response):
        if outcome == "miss":
            #logger.debug("Cache %s: %s\n%s" % (
            #    outcome,str(url),str(response)
            #))
            logger.debug("cache miss")

    def register_prefix(self,key,prefix):
        self.prefixes[key] = prefix

    def load_cache(self,filename):
        self.cache_filename = filename
        try :
            cache_file = open(self.cache_filename,"r")
            initial_cache_entries = cache_file.read().split('\n')
            cache_file.close()
            for cache_entry in initial_cache_entries :
                if cache_entry in ( "OVER_QUERY_LIMIT" ):
                    # Don't load entries like this
                    continue
                url,result = cache_entry.split('|',1)
                self.cache[url] = result

        except IOError:
            # Cache file did not exist but will be created
            # when first item is saved.
            pass

    def _url_to_cache_key(self,url):
        for key in self.prefixes.keys() :
            prefix = self.prefixes[key]
            if(url.startswith(prefix)):
                return url.replace(prefix,key,1)
        return url
    
    def get_response(self,url):
        cache_key = self._url_to_cache_key(url)
        response = None
        if cache_key in self.cache.keys() :
            response = self.cache[cache_key]
            self._cache_debug_message("hit",url,response)
        else :
            self.cache_misses += 1
            response =  _get_response_via_urllib2(url)
            self._cache_debug_message("miss",url,response)
        return response
    
    def cache_response(self,url,response):
        """Response caching is implemented as a separate function from
        getting a URL so that the URL requester can decide whether
        a particular response is sufficiently useful to cache"""
        cache_key = self._url_to_cache_key(url)
        if self.cache.get(cache_key,None) != response :
            self.cache[cache_key] = response
            if self.cache_filename :
                os = open(self.cache_filename,"ab")
                os.write(bytes("|".join((cache_key,response,)),'utf-8'))
                os.write(bytes('\n','utf-8'))
                os.close()

class ProxyFactory :
    def __init__(self,cache_filename = None):
        self.cache_filename = cache_filename
    def create_proxy(self):
        proxy = Proxy()
        if self.cache_filename :
            proxy.load_cache(self.cache_filename)
        return proxy

import unittest
import os
TEST_URL_PREFIX = 'https://maps.googleapis.com/maps/api/place/'
TEST_URL_COMPLETION = 'nearbysearch/json?lat=40.0&long=0.0'
TEST_CACHE_FILENAME = "proxy_test.cache"

class ProxyTest(unittest.TestCase) :
    
    def __init__(self,*args, **kwargs):
        self.dir = kwargs.pop("dir",".")
        self.cache_filename = kwargs.pop(
            "cache_filename",
            os.path.join(self.dir,"test.cache")
        )
        super(ProxyTest, self).__init__(*args, **kwargs)

    def remove_cache(self):
        try :
            os.remove(TEST_CACHE_FILENAME)
        except OSError :
            pass 
                   
    def setUp(self):
        self.remove_cache()

    def tearDown(self):
        # For the moment we leave the cache in existence so 
        # it can be examined.
        # self.remove_cache()
        pass
        
    def test_proxy(self) :
        # enable_debug()
        test_url = TEST_URL_PREFIX+TEST_URL_COMPLETION

        test_proxy1 = Proxy()
        test_proxy1.load_cache(TEST_CACHE_FILENAME)
        test_proxy1.register_prefix('gp:',TEST_URL_PREFIX) 
        
        resp = test_proxy1.get_response(test_url)
        
        # It is the responsibility of the caller to 
        # decide whether the response is good enough to cache.
        # The cache can't handle responses with newlines, so 
        # the caller needs to strip them out (usually OK with JSON,
        # may be problematic for some other response formats)
        resp = resp.replace('\n',' ')
        test_proxy1.cache_response(test_url,resp)
        self.assertEqual(1,test_proxy1.cache_misses)
        
        # Try to get the same item again.
        # It should be found in the cache, so cache_misses should
        # not increase.
        resp = test_proxy1.get_response(test_url)
        self.assertEqual(1,test_proxy1.cache_misses)      
        test_proxy1 = None

        # Create a new proxy, using the same cache file
        test_proxy2 = Proxy()
        test_proxy2.load_cache(TEST_CACHE_FILENAME)
        test_proxy2.register_prefix('gp:',TEST_URL_PREFIX) 
        test_proxy2.get_response(test_url)
        self.assertEqual(0,test_proxy2.cache_misses)
        test_proxy2 = None
        
        # Create a new proxy, not using a cache file
        test_proxy3 = Proxy()
        test_proxy3.register_prefix('gp:',TEST_URL_PREFIX) 
        test_proxy3.get_response(test_url)
        self.assertEqual(1,test_proxy3.cache_misses)
        test_proxy3 = None
        
    def runTest(self):
        logger.info("Running unit tests from class ProxyTest")
        self.test_proxy()

if __name__ == "__main__" :
    from unittest.runner import TextTestRunner
    logger.setLevel(logging.INFO)
    runner = TextTestRunner()
    runner.run(ProxyTest())
