# README #

### What is this repository for? ###

The code in this repository creates example documents in the .tqz format 
consumed by the iOS and Android Trailer applications.

Trailer for iOS: 
https://itunes.apple.com/us/app/tl-trailer/id511877626?mt=8&uo=4

Trailer for Android: 
https://play.google.com/store/apps/details?id=com.blogspot.tltrailer.androidapp

This script makes use of a library called python-cluster, maintained by Michel 
Albert, released via https://github.com/exhuma/python-cluster.  The upstream
code of this upstream library has been patched slightly to enable the usage in 
this integration to work under Python3.5.

The trailer-google-places-recommender library is released under the Lesser GNU 
Public Library version 2.1.

### Is it useful for anything else? ###

Other developers may find this library useful if they are interested in using 
the Google API's for map-based projects.  I have plans to add support for 
generation of files in Google's KML and KMZ file formats, and also possibly
files in the format consumed by the Apple's iOS Simulator application under
the Debug/Location menu option.

I've also used it as a place to experiment with automated test frameworks
and code coverage measurement, and others interested in this area may
find it a useful example.  The script coverage.sh runs the automated 
tests and reports the current statistics for the git commit ce1bf3.


~~~~
Name                    Stmts   Miss  Cover   Missing
-----------------------------------------------------
google_api_client         221     12    95%   161, 176-180, 189, 198, 214, 223, 245, 253, 262, 275-276
great_circle_distance      11      0   100%
proxy                     123      6    95%   17-19, 64, 108-109
recommender               152     13    91%   28, 36-37, 127-132, 150-152, 154, 190-191
simple_trail              123     23    81%   28, 49, 110-114, 129, 135, 138, 144-145, 150-151, 162-176
-----------------------------------------------------
TOTAL                     630     54    91%
~~~~

### How do I get set up? ###

To work on this repository, simply check it out on a macOS or Linux machine 
and then run the script gen_day_zero_trails.sh in the top level directory.
This script will generate .tqz files containing places of interest in a 
number of towns and cities in Scandinavia into the subdirectory generated_trails.

It should be possible to generate .tqz files for places elsewhere in the world
by editing python/gen_day_zero_trails.py.

For the script to run correctly, you will need to register with Google for a key
to use the Google Places API.  The key will be a 40 character string, which 
you will need to save in a file called google-places-api-key.txt in your 
home directory.

### Contribution guidelines ###

The primary purpose of this project is to support the iOS and Android applications
listed above, but contributors are welcome to fork the repository and use the generated 
files for other purposes if they are found useful.  I welcome pull requests, but I 
reserve the right to decline them (giving the contributor the courtesy of a reason) if 
they aren't compatible with the direction I want my own copy of the source to take.

Note that the code of the iOS and Android applications are not presently published.
I presently have no plan for these repositories to be made open for a number of reasons, 
including the fact that they are projects I did while I was learning the respective
sets of mobile idioms, and I wouldn't want my professional capabilities to be judged
on these bodies of source.

### Who do I talk to? ###

Please contact the Bitbucket repository owner, Tim Littlefair, if you are interested
in contributing back into the master repository.  Preferred mode of contact is to 
send a message via Bitbucket's messaging feature.

