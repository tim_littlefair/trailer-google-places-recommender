# coding: utf-8

import os
import logging

from context import trailer

from trailer import recommender, simple_trail, proxy
from trailer.template_tqz import TqzTemplate
from trailer.template_kml import KmlTemplate
from trailer.google_api_client import DEFAULT_PLACE_TYPES
from trailer.itinerary import generate_itinerary_trail

CLUSTER_AREA_NAMES = (
    "Stockholm, Sweden",
    "Gothenburg, Sweden",
    "Drammen, Norway",
    # "Røldal, Norway",
    "Odda, Norway",
    "Bergen, Norway",
    "Stavanger, Norway",
    "Grimstad, Norway",
    "Trollhätten, Sweden",

    "Oslo, Norway",
    "Trondheim, Norway",
    "Copenhagen, Denmark",
)

default_logging_level = logging.INFO
for logger_name in ("recommender", "google_api_client"):
    logging.getLogger(logger_name).setLevel(default_logging_level)
# logging.getLogger("recommender").setLevel(logging.DEBUG)

the_recommender = recommender.Recommender(
    simple_trail.TrailFactory(),
    proxy.ProxyFactory("_test/test.cache")
)

for cluster_area_name in CLUSTER_AREA_NAMES :
    trails = the_recommender.generate_clustered_trails(
            cluster_area_name, 
            search_radius_metres=25000,
            # types = DEFAULT_PLACE_TYPES,
            max_items_per_trail = 15
    )

    itinerary_trail = generate_itinerary_trail(
        "Tim and Kate's Sweden and Norway Trip, 2018",
        [
            "Stockholm, Sweden",
            "Gothenburg, Sweden",
            "Drammen, Norway",
            "Røldal, Norway",
            "Bergen, Norway",
            "Stavanger, Norway",
            "Grimstad, Norway",
            "Trollhättan, Sweden"
        ]
    )
    trails += [ itinerary_trail, ]

    gendir = os.path.join("_test","generated","samples")
    for t in trails :
        simple_trail.generate(t,TqzTemplate(),gendir)
        simple_trail.generate(t,KmlTemplate(),gendir)

