# google_places_client.py
# This module defines the PlacesClient class,
# The purpose of this class is to contain all knowledge of
# the Google Places API's scheme of URL parameters and 
# the content of requests from that API.
# This module relies on the proxy module to perform web
# requests and caching.

import os
import urllib
import json
import re
import sys
import logging

logger = logging.getLogger("google_api_client")
logger.setLevel(logging.INFO)

from great_circle_distance import great_circle_distance_in_metres

key_file = os.path.expanduser('~/foursquare-api-key.json')
try:
    key_json = open(key_file,'r').read().strip()
    key=json.loads(key_json)
except FileNotFoundError:#pragma nocover
    logger.error("""
In order to use this program, an client id and client secret 
from FourSquare are required.
These can be obtained by following the instructions at:
https://developer.foursquare.com/docs/api/getting-started.
Once you have the client id and client secret you need to store 
them in your computer in the file %s.""" % ( key_file,) 
)
    sys.exit(1)

GEONAMES_API_PREFIX = "http://api.geonames.org"

def geocode_locality_url(locality_string):
    return (
        "%s/searchJSON?name=%s&IsNameRequired=true&%s&username=%s"%(
        GEONAMES_API_PREFIX, 
        urllib.parse.quote_plus(bytes(locality_string,'utf-8')),
        "featureClass=P&featureClass=A",
        key["GeonamesUsername"], 
    ))

def reverse_geocode_latlng_url(lat,lng):
    return (
        "%s/findNearbyPlaceNameJSON?lat=%f&lng=%f&username=%s"%(
        GEONAMES_API_PREFIX, lat, lng, key["GeonamesUsername"], 
    ))

PLACES_API_NEARBY_URL_PREFIX = (
    'https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=%s' % (
        key,
    )
)
def places_nearby_url(lat,lng,radius=5000):
    return (
        "%s&location=%.6f,%.6f&radius=%d&rankby=prominence" % (
            PLACES_API_NEARBY_URL_PREFIX,lat,lng,radius
        )
    )

PLACES_API_DETAILS_URL_PREFIX = (
    'https://maps.googleapis.com/maps/api/place/details/json?key=%s' % (
        key,
    )
)
def places_details_url(placeid):
    return (
        "%s&placeid=%s" % (
            PLACES_API_DETAILS_URL_PREFIX,placeid
        )
    )

DEFAULT_PLACE_TYPES = (
    "art_gallery", "museum", "park", "zoo", "library", "movie_theater",
)

MINIMUM_ZONE_RADIUS = 150

RESULTS_KEY = 'results'
RESULT_KEY = 'result'
GEOMETRY_KEY = 'geometry'
BOUNDS_KEY = 'bounds'
LOCATION_KEY = 'location'
LAT_KEY = 'lat'
LNG_KEY = 'lng'
VIEWPORT_KEY = 'viewport'
SWLOC_KEY = 'southwest'
NELOC_KEY = 'northeast'
PLACEID_KEY = 'place_id'
NAME_KEY = 'name'
TYPES_KEY = 'types'
WEBSITE_KEY = 'website'
STATUS_KEY = 'status'
VICINITY_KEY = 'vicinity'
FORMATTED_ADDRESS_KEY = 'formatted_address'

class FoursquareApiClient :
    def __init__(self,proxy):
        self.proxy = proxy
        self.proxy.register_prefix("gg:",GEONAMES_API_PREFIX)
        self.proxy.register_prefix("gpn:",PLACES_API_NEARBY_URL_PREFIX)
        self.proxy.register_prefix("gpd:",PLACES_API_DETAILS_URL_PREFIX)
    
    def _get_latlngrad(self, place_result,default_radius=0):
        return float(place_result["lat"]),float(place_result["lng"]),5000
        geometry = place_result[GEOMETRY_KEY]
        lat = geometry[LOCATION_KEY][LAT_KEY]
        lng = geometry[LOCATION_KEY][LNG_KEY]
        radius = default_radius
        for key in BOUNDS_KEY, VIEWPORT_KEY:
            if key in geometry:
                corners = geometry[key]
                neloc = corners[NELOC_KEY]
                swloc = corners[SWLOC_KEY]
                radius = int(
                    great_circle_distance_in_metres(
                        (neloc[LAT_KEY], neloc[LNG_KEY]), 
                        (swloc[LAT_KEY], swloc[LNG_KEY])
                    ) / 2.0
                )
                break
        return (lat,lng,radius)

    def _get_formatted_address(self, reverse_geocode_results):
        DEFAULT_ADDRESS_TYPES = ( "locality", )
        retval = None 
        for result in reverse_geocode_results :
            result_address = result[FORMATTED_ADDRESS_KEY]
            result_types = result[TYPES_KEY]
            # logger.debug(
            #    "Reverse geocode result: %s: %s" % (
            #        result_types, result_address
            #    )
            # )
            for address_type in DEFAULT_ADDRESS_TYPES :
                if address_type in result_types :
                    retval = result_address
                    # logger.debug(u"%s in %s"%(address_type,result_types))
                    break
                else :
                    # logger.debug(u"%s not in %s"%(address_type,result_types))
                    pass
            if retval is not None :
                break
        # TODO: Sometimes names start with the same component repeated, 
        # e.g. Middlesborough, Middlesborough, UK.
        # This is ugly, think about implementing an RE 
        # search to fix it up.
        logger.debug("Selected name is %s"%(retval,))
        return retval
    def _validate_response(self,url):
        response = self.proxy.get_response(url)
        if response is None:
            return None
        # print(response,file=sys.stderr)
        # The response is expected to be a JSON document.
        # Replacing newlines with spaces is necessary so that the result can
        # be cached.
        # Collapsing whitespace after newlines also helps to make the 
        # response shorter, both for caching and consumption purposes
        response_json = re.sub(r"\n\s*", " ", response)
        response_dict = json.loads(response_json)
        if response_dict["totalResultsCount"] >= 1 :
            self.proxy.cache_response(url, response_json)
            return response_dict
        else:#pragma nocover
            logging.error(
                "failed request to url:%s\nresponse:\n%s\nheaders:%s\n" % (
                url, response, ""
            ))
            return None

    def _combine_result_arrays(self,result_arrays):
        results = []
        while len(result_arrays) > 0:
            a = result_arrays[0]
            result_arrays = result_arrays[1:]
            if len(a)==0:
                continue
            else:
                #logger.debug("Result arrays: %d this array: %d results: %d" %
                #    ( len(result_arrays), len(a), len(results))
                #)
                while a[0] in results:
                    a = a[1:]
                    if len(a)==0:
                        break
                if len(a)>0 and a[0] not in results:
                    results += [ a[0] ]
                    a = a[1:]
                result_arrays += [ a ]
        return results

    def get_locality_latlngrad(self,locality_string):
        url = geocode_locality_url(locality_string)
        response = self._validate_response(url)
        if response is not None:
            result0 = response["geonames"][0]
            return self._get_latlngrad(result0)
        else :
            return None      

    def get_latlng_placename(self,lat,lng,bounds_rect=None):
        url = reverse_geocode_latlng_url(lat,lng)
        response = self._validate_response(url)
        if response is not None:
            results = response[RESULTS_KEY]
            return self._get_formatted_address(results)
        else :
            return None      

    def get_nearby_place_latlng(self,lat,lng,radius,max_results=0,requested_types=None):
        url = places_nearby_url(lat, lng, radius) 
        results = []
        if requested_types is not None:
            result_arrays = []
            for t in requested_types:
                url_for_type = url + "&type=" + t
                try:
                    response_for_type = self._validate_response(url_for_type)
                    result_arrays += [ response_for_type[RESULTS_KEY] ]
                    logging.debug("Returned %d results for type %s"%(
                        len(response_for_type),t
                    )) 
                except:
                    continue
            results = self._combine_result_arrays(result_arrays)
        else:
            places_response = self._validate_response(url)            
            results = places_response[RESULTS_KEY]
        if max_results is not None and len(results) > max_results :
            results = results[0:max_results]
        result_map = {}
        for result in results:
            place_id = result[PLACEID_KEY]
            lat, lng, radius = self._get_latlngrad(result, MINIMUM_ZONE_RADIUS)
            place_coordinates = ( lat, lng, radius )
            result_map[place_id] = place_coordinates
        return result_map

    def get_nearby_place_details(self,lat,lng,radius,max_results=None,requested_types=None):
        url = places_nearby_url(lat,lng,radius)
        results = []
        if requested_types is not None:
            result_arrays = []
            for t in requested_types:
                url_for_type = url + "&type=" + t
                try:
                    response_for_type = self._validate_response(url_for_type)
                    result_arrays += [ response_for_type[RESULTS_KEY] ] 
                except:
                    continue
            results = self._combine_result_arrays(result_arrays)
        else:
            places_response = self._validate_response(url)            
            if places_response is not None :
                results = places_response[RESULTS_KEY]
        logger.debug("get_nearby_place_details: %d results returned"%(len(results)))
        if max_results is not None and len(results) > max_results :
           results = results[0:max_results]
        result_map = {}
        for result in results:
            place_id = result[PLACEID_KEY]
            place_details = self.get_place_details(place_id)
            if place_details is not None :
                result_map[place_id] = place_details
        return result_map

    def get_place_details(self, place_id):
        place_response = self._validate_response(places_details_url(place_id))
        if place_response is not None:
            result = place_response[RESULT_KEY]
            lat, lng, radius = self._get_latlngrad(result, MINIMUM_ZONE_RADIUS)
            if radius < MINIMUM_ZONE_RADIUS:
                radius = MINIMUM_ZONE_RADIUS
            name = result[NAME_KEY]
            types = result.get(TYPES_KEY, ())
            website = result.get(WEBSITE_KEY, '')
            geometry = result.get(GEOMETRY_KEY, ())
            vicinity = result.get(VICINITY_KEY, '')
            formatted_address = result.get(FORMATTED_ADDRESS_KEY, '')
            return lat, lng, radius, name, types, website, vicinity, formatted_address, geometry
        else :
            return None

import unittest
import proxy

TEST_CACHE_FILENAME = "google_api_client_test.cache"

class FoursquareApiClientTest(unittest.TestCase) :
               
    def setUp(self):
        self.total_cache_misses = 0
    
    def tearDown(self):
        # For the moment we leave the cache in existence so 
        # it can be examined.
        # self.remove_cache()
        pass
    
    def do_test_client(self,proxy) :
        client = FoursquareApiClient(proxy)

        NEWCASTLE  = (54.9782,-1.6177,"Newcastle upon Tyne")
        DURHAM     = (54.7752,-1.5848,"Durham")
        GOTHENBURG = (57.7088,11.9745,"Gothenburg")
        # SUNDERLAND = (54.89469855,-1.4208559,"Sunderland, UK")
        # NORTH_SHIELDS = (55.00411705,-1.4803505,"North Shields, UK")
        # FREMANTLE = (-32.04,+115.75,"North Fremantle WA 6159, Australia")
        # MIDDLESBROUGH = (54.55720765,-1.2373965, "Middlesbrough, UK")
        for latlngname in ( 
            NEWCASTLE, DURHAM, GOTHENBURG,
            # SUNDERLAND, 
            # NORTH_SHIELDS,
            # MIDDLESBROUGH, 
            # FREMANTLE,
        ) :       
            ref_lat, ref_lng, ref_name = latlngname
            # Geocode from a locality name
            (lat,lng,rad) = client.get_locality_latlngrad(ref_name)
            self.assertEqual(float,type(lat))
            self.assertEqual(float,type(lng))
            self.assertEqual(int,type(rad))
            logger.debug(
                "Geocode returns coordinates %f,%f with radius %d metres for locality %s"%(
                lat,lng,rad,ref_name
            ))
            # The coordinates returned may vary over time,
            # but probably by no more than a kilometer
            distance_from_reference = great_circle_distance_in_metres(
                (ref_lat,ref_lng),(lat,lng),
            )
            logger.debug("Distance from reference point is %d metres" % (distance_from_reference,))
            self.assertTrue(distance_from_reference<=1000)
            # Stuff beyond this point is TODO
            continue

            # Find points of interest in or near the specified locality
            # without requesting their details
            nearby_places = client.get_nearby_place_latlng(
                lat,lng,rad,max_results=10,
                requested_types=DEFAULT_PLACE_TYPES
            )
            self.assertTrue(len(nearby_places)>0)
            for p in nearby_places.values() :
                self.assertEqual(float,type(p[0]))
                self.assertEqual(float,type(p[1]))

            # Find points of interest in or near the specified locality
            nearby_place_details = client.get_nearby_place_details(
                lat,lng,rad,max_results=10,
                requested_types=DEFAULT_PLACE_TYPES
            )
            self.assertTrue(len(nearby_place_details)>0)
            for k in nearby_place_details.keys() :
                p = nearby_place_details[k]
                logger.debug("placeid: %s: name: %s types:%s"%(
                    k,p[3],p[4]
                ))
                self.assertEqual(float,type(p[0]))
                self.assertEqual(float,type(p[1]))

            # Do a reverse geocode to check the reference coordinates 
            # supplied map back to the locality name
            self.assertEqual(ref_name,client.get_latlng_placename(ref_lat, ref_lng))
            
    def test_client_twice(self):
        # The first time we run do_test_client
        # we don't know whether the results are cached
        # but if we run it a second time, every URL should
        # be in the cache file and there should be no more
        # cache misses
        
        proxy.enable_debug()
        proxy1 = proxy.Proxy()
        proxy1.load_cache(TEST_CACHE_FILENAME)
        self.do_test_client(proxy1)
        cache_misses_after_first_run = proxy1.cache_misses
        self.total_cache_misses += cache_misses_after_first_run

        proxy2 = proxy.Proxy()
        proxy2.load_cache(TEST_CACHE_FILENAME)
        self.do_test_client(proxy2)
        cache_misses_after_second_run = proxy2.cache_misses
        self.total_cache_misses += cache_misses_after_second_run
        logger.info(
            "Cache  misses: first run:%d second run: %d" % ( 
                cache_misses_after_first_run, 
                cache_misses_after_second_run
            )
        )
        # self.assertEqual(0,cache_misses_after_second_run)
        
    def runTest(self):
        logger.info("Running unit tests from class GoogleApiTest")        
        self.test_client_twice()

if __name__ == "__main__" :
    from unittest.runner import TextTestRunner
    logger.setLevel(logging.DEBUG)
    runner = TextTestRunner()
    runner.run(FoursquareApiClientTest())
