# google_places_client.py
# This module defines the PlacesClient class,
# The purpose of this class is to contain all knowledge of
# the Google Places API's scheme of URL parameters and 
# the content of requests from that API.
# This module relies on the proxy module to perform web
# requests and caching.

import os
import urllib
import json
import re
import sys
import logging

logger = logging.getLogger("google_api_client")
logger.setLevel(logging.INFO)

from .great_circle_distance import great_circle_distance_in_metres
from .simple_trail import Zone as ZoneCandidate

key_file = os.path.expanduser('~/google-places-api-key.txt')
try:
    key = open(key_file,'r').readline().strip()
except FileNotFoundError:#pragma nocover
    logger.error("""
In order to use this program, an API key from Google is required.
The key can be obtained by signing in to Google and visiting:
https://console.cloud.google.com/apis/credentials.
Once you have the key you need to store it in your computer in 
the file %s.""" % ( key_file,) 
)
    sys.exit(1)

GEOCODE_API_URL_PREFIX = (
    'https://maps.googleapis.com/maps/api/geocode/json?key=%s' % (
        key,
    )
)
def geocode_locality_url(locality_string):
    return (
        "%s&address=%s" %
        (GEOCODE_API_URL_PREFIX,urllib.parse.quote_plus(bytes(locality_string,'utf-8')))
    )

def reverse_geocode_latlng_url(lat,lng):
    return (
        "%s&latlng=%s" % (
            GEOCODE_API_URL_PREFIX,
            urllib.parse.quote_plus(",".join((str(lat),str(lng)))),
            # urllib.quote_plus("|".join(address_types))
        )
    )

PLACES_API_NEARBY_URL_PREFIX = (
    'https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=%s' % (
        key,
    )
)
def places_nearby_url(lat,lng,radius=5000):
    return (
        "%s&location=%.6f,%.6f&radius=%d&rankby=prominence" % (
            PLACES_API_NEARBY_URL_PREFIX,lat,lng,radius
        )
    )

PLACES_API_TEXT_URL_PREFIX = (
    'https://maps.googleapis.com/maps/api/place/textsearch/json?key=%s' % (
        key,
    )
)
def places_text_url(lat,lng,radius=5000):
    return (
        "%s&location=%.6f,%.6f&radius=%d&rankby=prominence" % (
            PLACES_API_TEXT_URL_PREFIX,lat,lng,radius
        )
    )

PLACES_API_DETAILS_URL_PREFIX = (
    'https://maps.googleapis.com/maps/api/place/details/json?key=%s' % (
        key,
    )
)
def places_details_url(placeid):
    return (
        "%s&placeid=%s" % (
            PLACES_API_DETAILS_URL_PREFIX,placeid
        )
    )

DEFAULT_PLACE_TYPES = (
    "park", "museum", "zoo",
    "live+music*","live+theatre*","live+dance*",
    "botanical+garden*",
)

EXCLUDED_PLACE_TYPES = ( "lodging", "campground", "restaurant", "store", )

RESULTS_KEY = 'results'
RESULT_KEY = 'result'
GEOMETRY_KEY = 'geometry'
BOUNDS_KEY = 'bounds'
LOCATION_KEY = 'location'
LAT_KEY = 'lat'
LNG_KEY = 'lng'
VIEWPORT_KEY = 'viewport'
SWLOC_KEY = 'southwest'
NELOC_KEY = 'northeast'
PLACEID_KEY = 'place_id'
NAME_KEY = 'name'
TYPES_KEY = 'types'
WEBSITE_KEY = 'website'
STATUS_KEY = 'status'
VICINITY_KEY = 'vicinity'
FORMATTED_ADDRESS_KEY = 'formatted_address'

class GoogleApiClient :
    def __init__(self,proxy):
        self.proxy = proxy
        self.proxy.register_prefix("gg:",GEOCODE_API_URL_PREFIX)
        self.proxy.register_prefix("gpn:",PLACES_API_NEARBY_URL_PREFIX)
        self.proxy.register_prefix("gpt:",PLACES_API_TEXT_URL_PREFIX)
        self.proxy.register_prefix("gpd:",PLACES_API_DETAILS_URL_PREFIX)
        self.nearby_results = {}
        self.detailed_results = {}
        self.candidate_zones = {}

    def _get_latlngrad(self, place_result, default_radius=0):
        lat,lng = self.coords_for_result(place_result)
        radius = self.radius_for_result(place_result)
        return (lat,lng,radius)

    def _get_formatted_address(self, reverse_geocode_results):
        DEFAULT_ADDRESS_TYPES = ( "locality", )
        retval = None 
        for result in reverse_geocode_results :
            result_address = result[FORMATTED_ADDRESS_KEY]
            result_types = result[TYPES_KEY]
            # logger.debug(
            #    "Reverse geocode result: %s: %s" % (
            #        result_types, result_address
            #    )
            # )
            for address_type in DEFAULT_ADDRESS_TYPES :
                if address_type in result_types :
                    retval = result_address
                    # logger.debug(u"%s in %s"%(address_type,result_types))
                    break
            if retval is not None :
                break
        # TODO: Sometimes names start with the same component repeated, 
        # e.g. Middlesborough, Middlesborough, UK.
        # This is ugly, think about implementing an RE 
        # search to fix it up.
        logger.debug("Selected name is %s"%(retval,))
        return retval

    def _validate_response(self,url):
        response = self.proxy.get_response(url)
        # The response is expected to be a JSON document.
        response_dict = json.loads(response)
        # Filter out some parts of the response we will never care about
        # This helps to keep the cache smaller and makes it easier to use
        # for optimizing exploitation of results
        response_dict.pop("next_page_token",None)
        results = []
        if RESULT_KEY in response_dict:
            results += [ response_dict[RESULT_KEY] ]
        elif RESULTS_KEY in response_dict:
            results += response_dict[RESULTS_KEY]
        for r in results:
            for key in ( "icon", "opening_hours", "photos", "reviews", "reference"):
                r.pop(key,None)
        # Replacing newlines with spaces is necessary so that the result can
        # be cached.
        # Collapsing whitespace after newlines also helps to make the 
        # response shorter, both for caching and consumption purposes
        response_json = re.sub(r"\n\s*", " ", json.dumps(response_dict))
        if response_dict[STATUS_KEY] == 'OK':
            self.proxy.cache_response(url, response_json)
            return response_dict
        elif response_dict[STATUS_KEY] == 'ZERO_RESULTS':
            self.proxy.cache_response(url, response_json)
            return response_dict
        elif response_dict[STATUS_KEY] == 'OVER_QUERY_LIMIT':
            self.proxy.cache_response(url, response_json)
            return response_dict
        else:#pragma nocover
            logging.error(
                "failed request to url:%s\nresponse:\n%s\n" % (
                url, response,
            ))
            return None

    def get_locality_latlngrad(self,locality_string):
        url = geocode_locality_url(locality_string)
        response = self._validate_response(url)
        if response is not None:
            result0 = response[RESULTS_KEY][0]
            return self._get_latlngrad(result0)
        else :
            return None      

    def get_locality_id(self,locality_string):
        url = geocode_locality_url(locality_string)
        response = self._validate_response(url)
        if response is not None:
            result0 = response[RESULTS_KEY][0]
            return result0[PLACEID_KEY]
        else :
            return None      

    def get_latlng_placename(self,lat,lng,bounds_rect=None):
        url = reverse_geocode_latlng_url(lat,lng)
        response = self._validate_response(url)
        if response is not None:
            results = response[RESULTS_KEY]
            return self._get_formatted_address(results)
        else :
            return None      

    def get_places_near_latlng(self,lat,lng,radius,requested_types=None):
        nearby_url = places_nearby_url(lat, lng, radius) 
        text_url = places_text_url(lat,lng,radius)
        result_arrays = []
        if requested_types is not None:
            for t in requested_types:
                url_for_type = None
                if t.endswith("*"):
                    url_for_type = text_url + "&query=" + t.replace("*","")
                else:
                    url_for_type = nearby_url + "&type=" + t
                response_for_type = self._validate_response(url_for_type)
                result_arrays += [ response_for_type[RESULTS_KEY] ]
                logging.debug("Returned %d results for type %s"%(
                    len(response_for_type),t
                )) 
        else:
            places_response = self._validate_response(nearby_url)            
            result_arrays += [ places_response[RESULTS_KEY] ]
            requested_types = ["*"]
        return result_arrays

    def get_place_details(self, place_id):
        place_response = self._validate_response(places_details_url(place_id))
        if place_response is not None and RESULT_KEY in place_response:
            result = place_response[RESULT_KEY]
            return result
        else:
            return None

    def uid_for_result(self,result):
        return result[PLACEID_KEY]

    def name_for_result(self,result):
        return result[NAME_KEY]
        
    def coords_for_result(self,result):
        geometry = result[GEOMETRY_KEY]
        lat = round(float(geometry[LOCATION_KEY][LAT_KEY]),6)
        lng = round(float(geometry[LOCATION_KEY][LNG_KEY]),6)
        return lat, lng
    
    def radius_for_result(self,result):     
        radius = 0
        geometry = result[GEOMETRY_KEY]
        for key in BOUNDS_KEY, VIEWPORT_KEY:
            if key in geometry:
                corners = geometry[key]
                neloc = corners[NELOC_KEY]
                swloc = corners[SWLOC_KEY]
                radius = int(
                    great_circle_distance_in_metres(
                        (neloc[LAT_KEY], neloc[LNG_KEY]), 
                        (swloc[LAT_KEY], swloc[LNG_KEY])
                    ) / 2.0
                )
                if key == VIEWPORT_KEY:
                    # Google's viewports are intended to show the 
                    # location with surrounding context 
                    # than the target place, so reduce the radius
                    # by a factor to allow for this
                    radius /= 3
                break
        return radius

    def zone_attrs_for_result(self,result):
        name = result[NAME_KEY]
        lat, lng = self.coords_for_result(result)
        radius = self.radius_for_result(result)
        url = result.get(WEBSITE_KEY, '')
        address = result.get(FORMATTED_ADDRESS_KEY, '')
        message = "Google Places types: " + ", ".join(result.get(TYPES_KEY,[]))
        """
        if radius>MAXIMUM_ABSOLUTE_ZONE_RADIUS:
            # The place is too large for us to wish to consider it as a zone
            logger.info("Zone too large: %s with radius %d metres"%(
                radius,name
            ))
            return None
        elif radius < MINIMUM_ZONE_RADIUS:
            radius = MINIMUM_ZONE_RADIUS
        elif radius > MAXIMUM_VISUAL_ZONE_RADIUS:
            radius = MAXIMUM_VISUAL_ZONE_RADIUS
        """
        return  name, lat, lng, radius, url, address, message

import unittest
from . import proxy

TEST_CACHE_FILENAME = "google_api_client_test.cache"

# Test data
NEWCASTLE  = (54.9782,-1.6177,"Newcastle upon Tyne, UK")
DURHAM     = (54.7752,-1.5848,"Durham, UK")
GOTHENBURG = (57.7088,11.9745,"Gothenburg, Sweden")
# SUNDERLAND = (54.89469855,-1.4208559,"Sunderland, UK")
# NORTH_SHIELDS = (55.00411705,-1.4803505,"North Shields, UK")
# FREMANTLE = (-32.04,+115.75,"North Fremantle WA 6159, Australia")
# MIDDLESBROUGH = (54.55720765,-1.2373965, "Middlesbrough, UK")

class GoogleApiClientTest(unittest.TestCase) :
               
    def __init__(self,*args, **kwargs):
        self.dir = kwargs.pop("dir",".")
        self.cache_filename = kwargs.pop(
            "cache_filename",
            os.path.join(self.dir,"test.cache")
        )
        super(GoogleApiClientTest, self).__init__(*args, **kwargs)
         
    def setUp(self):
        self.total_cache_misses = 0
    
    def tearDown(self):
        # For the moment we leave the cache in existence so 
        # it can be examined.
        # self.remove_cache()
        pass
    
    def do_test_client(self,proxy) :
        client = GoogleApiClient(proxy)

        for latlngname in ( 
            NEWCASTLE, DURHAM, GOTHENBURG,
            # SUNDERLAND, 
            # NORTH_SHIELDS,
            # MIDDLESBROUGH, 
            # FREMANTLE,
        ) :       
            ref_lat, ref_lng, ref_name = latlngname
            # Geocode from a locality name
            (lat,lng,rad) = client.get_locality_latlngrad(ref_name)
            self.assertEqual(float,type(lat))
            self.assertEqual(float,type(lng))
            self.assertEqual(int,type(rad))
            logger.debug(
                "Geocode returns coordinates %f,%f with radius %d metres for locality %s"%(
                lat,lng,rad,ref_name
            ))
            # The coordinates returned may vary from call to call,
            # but probably by no more than a kilometer
            distance_from_reference = great_circle_distance_in_metres(
                (ref_lat,ref_lng),(lat,lng),
            )
            logger.debug("Distance from reference point is %d metres" % (distance_from_reference,))
            self.assertTrue(distance_from_reference<=1000)

            # Do a reverse geocode
            placename = client.get_latlng_placename(*latlngname[0:2])
            self.assertEqual(latlngname[2],placename)

            for requested_types in None, DEFAULT_PLACE_TYPES:
                # Find points of interest in or near the specified locality
                # without requesting their details
                nearby_place_results = client.get_places_near_latlng(
                    lat,lng,rad,
                    requested_types=requested_types
                )
                if requested_types is None:
                    self.assertEqual(1, len(nearby_place_results))
                else :                
                    self.assertEqual(len(requested_types), len(nearby_place_results))
                for results_for_type in nearby_place_results:
                    self.assertEquals(list,type(results_for_type))
                    for result in results_for_type:
                        self.assertEqual(dict,type(result))
                        self.assertTrue(PLACEID_KEY in result.keys())
                
    def test_client_twice(self):
        # The first time we run do_test_client
        # we don't know whetherthe results are cached
        # but if we run it a second time, every URL should
        # be in the cache file and there should be no more
        # cache misses
        
        proxy1 = proxy.Proxy()
        proxy1.load_cache(TEST_CACHE_FILENAME)
        self.do_test_client(proxy1)
        cache_misses_after_first_run = proxy1.cache_misses
        self.total_cache_misses += cache_misses_after_first_run

        proxy.enable_debug()
        proxy2 = proxy.Proxy()
        proxy2.load_cache(TEST_CACHE_FILENAME)
        self.do_test_client(proxy2)
        cache_misses_after_second_run = proxy2.cache_misses
        self.total_cache_misses += cache_misses_after_second_run
        logger.info(
            "Cache  misses: first run:%d second run: %d" % ( 
                cache_misses_after_first_run, 
                cache_misses_after_second_run
            )
        )
        # self.assertEqual(0,cache_misses_after_second_run)
        
    def runTest(self):
        logger.info("Running unit tests from class GoogleApiTest")        
        self.test_client_twice()

if __name__ == "__main__" :
    from unittest.runner import TextTestRunner
    logger.setLevel(logging.INFO)
    runner = TextTestRunner()
    runner.run(GoogleApiClientTest())
