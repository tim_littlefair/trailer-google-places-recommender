# Great circle distance calculation adapted from
# https://gist.github.com/gabesmed/1826175
import math
 
def great_circle_distance_in_metres(latlong_a, latlong_b):
    EARTH_CIRCUMFERENCE_IN_METRES = 6378137     # earth circumference in meters
    lat1, lon1 = latlong_a
    lat2, lon2 = latlong_b 
    dLat = math.radians(lat2 - lat1)
    dLon = math.radians(lon2 - lon1)
    a = (math.sin(dLat / 2) * math.sin(dLat / 2) +
            math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) * 
            math.sin(dLon / 2) * math.sin(dLon / 2))
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    d = EARTH_CIRCUMFERENCE_IN_METRES * c   
    return d
